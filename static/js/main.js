function img_listener(event) {
    const reader = new FileReader()
    reader.onload = get_mask;
    //reader.readAsText(event.target.files[0])
    reader.readAsDataURL(event.target.files[0]); 
}

function get_mask(event) {
    console.log(event.target.result)
    // var wait = true
    const request = new XMLHttpRequest();
    request.open('POST', '/api/get_mask');
    request.addEventListener("readystatechange", () => {
           if (request.readyState === 4 && request.status === 200) {
           
             // выводим в консоль то что ответил сервер
             server_response = request.response;

             image_src = "data:image/jpeg;base64," + server_response
             display_img(image_src)

            //  wait = false
             //event.target.result
             //request.responseText
             //document.getElementById('fileContent').textContent = event.target.result;           
           }
       });
    request.send(event.target.result);
    // while (wait) {
    //     console.log("LOOOOOOOOL")
    //     //document.getElementById
    // }
    //request.setRequestHeader('Content-Type', 'application/x-www-form-url');
}

function init() {
    document.getElementById('fileInput').addEventListener('change', img_listener, false);
 }

function display_img(image_src) {
    // Get the modal
    var modal = document.getElementById("myModal");
    
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = image_src;
    //captionText.innerHTML = 'txt src';
    
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
    modal.style.display = "none";
    }  
}
