# TrashDetectionApp

## Запуск

Для запуска сервера необходим получить ключи от хранилищ, затем выполнить команду:
- sudo docker-compose -f ./docker-compose.yml run --rm app

## Демо

Пример изображения в папке img
![Demo](/img/demo.gif)