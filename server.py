import base64
import os
import cv2
import mlflow
import numpy as np
import onnxruntime
import mlflow
from flask import Flask, make_response, render_template, request
from inference import inference  # noqa: E402 # pylint: disable=C0413

app = Flask(__name__)
ort_session = None  # pylint: disable=C0103


@app.route("/")
def upload_form():
    return render_template("index.html")


@app.route("/api/get_mask", methods=["POST"])
def get_mask():

    data = request.data.decode("utf-8")
    _, data = data.split(",", 1)
    image_data = base64.b64decode(data)
    np_array = np.frombuffer(image_data, np.uint8)
    image = cv2.imdecode(np_array, cv2.IMREAD_UNCHANGED)

    result = inference(ort_session, image)

    _, buffer_img = cv2.imencode(".jpg", result)
    data = base64.b64encode(buffer_img)
    response = make_response(data)
    response.headers["Content-Type"] = "image/jpeg"
    return response


if __name__ == "__main__":

    mlflow.set_tracking_uri(os.environ['SERVER_URL'])
    model_name = "test_model"
    model_stage = "Staging";
    uri = f"models:/{model_name}/{model_stage}"
    model = mlflow.onnx.load_model(uri, dst_path="onnxruntime")
    ort_session = onnxruntime.InferenceSession(
            model.SerializeToString(), providers=['CPUExecutionProvider']
    )

    app.run(host="0.0.0.0")
