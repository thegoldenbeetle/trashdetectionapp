import cv2
import numpy as np

MEAN = [0.485, 0.456, 0.406]
STD = [0.229, 0.224, 0.225]


def inference(
    ort_session, image, model_input_size=(384, 384), threshold=0.5, alpha=0.5
):
    height, width, _ = image.shape
    image_res = cv2.resize(image, model_input_size)
    im_rgb = cv2.cvtColor(image_res, cv2.COLOR_BGR2RGB)
    im_norm = (im_rgb - MEAN) / STD
    transposed = np.expand_dims(np.transpose(im_norm, (2, 0, 1)), axis=0)

    outputs = ort_session.run(None, {"input": transposed.astype(np.float32)})
    outputs = np.array(outputs).squeeze(axis=0).squeeze(axis=0)
    mask = outputs[0] > threshold

    res = np.full((*model_input_size, 3), (0, 0, 255))
    res[mask.astype(int) == 0] = (0, 0, 0)
    res = np.array(res, dtype="uint8")
    res = cv2.resize(res, (width, height), interpolation=cv2.INTER_NEAREST)

    return cv2.addWeighted(res, alpha, image, 1 - alpha, 0)
